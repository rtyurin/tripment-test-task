# Tripment test task

#### The goal is to make the page look like exactly as in design, with filtering and sorting possibilities.
[More info](https://github.com/tripment/test-tasks/tree/master/tripment-frontend).

To run this app use:


  1. `yarn` to install dependencies
  2. `yarn dev` to run the application


[See it live here](https://tripment-test-task.vercel.app/)
