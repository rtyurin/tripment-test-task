import { createSelector } from "@reduxjs/toolkit";
import { Doctor, Store } from "@/store/store";
import { Availability, Filters } from "@/reducers/filtersReducer";
import { compose } from "@reduxjs/toolkit";
import { addDays, isToday, isWithinInterval } from "date-fns";

const selectDoctors = (state: Store) => state.doctors.items;
const selectFilters = (state: Store) => state.filters;

export const patternByAvailabilityFilter = {
  today: (doctor: Doctor) => {
    const availableDate = new Date(doctor.offline_available);
    return isToday(availableDate);
  },
  within3days: (doctor: Doctor) => {
    const today = new Date();
    return isWithinInterval(new Date(doctor.offline_available), {
      start: today,
      end: addDays(today, 3),
    });
  },
  within2weeks: (doctor: Doctor) => {
    const today = new Date();
    return isWithinInterval(new Date(doctor.offline_available), {
      start: today,
      end: addDays(today, 14),
    });
  },
  telehealth: (doctor: Doctor) => doctor.telehealth,
  acceptNew: (doctor: Doctor) => doctor.acceptNew,
};

export const filterDoctorsBySpecificFilter = (
  filter: Filters["insurances"] | Filters["specialities"],
  filterName: "insurances" | "speciality"
) => (doctors: Doctor[]): Doctor[] => {
  if (filter.length === 0) {
    return doctors;
  }

  return doctors.filter((doctor: Doctor) =>
    filter.includes(doctor[filterName])
  );
};

export const filterDoctorsByAvailability = (
  availabilityFilter: Availability
) => (doctors: Doctor[]) => {
  const filterKeys = Object.keys(availabilityFilter);
  const turnedOnFilters = filterKeys.filter((key) => availabilityFilter[key]);

  if (!turnedOnFilters.length) {
    return doctors;
  }

  return doctors.filter((doctor: Doctor) => {
    return turnedOnFilters.every((key) =>
      patternByAvailabilityFilter[key](doctor)
    );
  });
};

export function applyAllFilters(doctors: Doctor[], filters: Filters) {
  const applyFilter = compose(
    filterDoctorsBySpecificFilter(filters.insurances, "insurances"),
    filterDoctorsBySpecificFilter(filters.specialities, "speciality"),
    filterDoctorsByAvailability(filters.availability)
  );

  return applyFilter(doctors);
}

export const filteredDoctorsSelector = createSelector(
  [selectDoctors, selectFilters],
  applyAllFilters
);
