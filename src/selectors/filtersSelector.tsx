import { createSelector } from "@reduxjs/toolkit";
import { Doctor, Store } from "@/store/store";
import React from "react";
import { Availability } from "@/reducers/filtersReducer";
import { VideoVisit } from "@/components/VideoVisit/VideoVisit";
import { addDays, format, isToday, isWithinInterval } from "date-fns";
import { patternByAvailabilityFilter } from "@/selectors/doctorsSelector";

const selectDoctors = (state: Store) => state.doctors.items;
const selectInsurancesFilter = (state: Store) => state.filters.insurances;
const selectSpecialitiesFilter = (state: Store) => state.filters.specialities;
const selectAvailabilityFilter = (state: Store) => state.filters.availability;

export interface DropdownFilterItem {
  title: string;
  id: string;
  checked: boolean;
  icon?: React.ReactNode;
  hasBorder?: boolean;
  count?: number;
}

function getItemsWithCount(
  doctors: Doctor[],
  filters: string[],
  property: "speciality" | "insurances"
): DropdownFilterItem[] {
  const countByProperty: { [property: string]: number } = doctors.reduce(
    (filtersBySpeciality, doctor) => {
      return {
        ...filtersBySpeciality,
        [doctor[property]]: filtersBySpeciality[doctor[property]]
          ? filtersBySpeciality[doctor[property]] + 1
          : 1,
      };
    },
    {}
  );

  const differentProperties = Object.keys(countByProperty).filter(Boolean);

  return differentProperties.map((name) => ({
    title: name,
    id: name,
    count: countByProperty[name],
    checked: filters.includes(name),
  }));
}

const availabilityFilters: Partial<DropdownFilterItem>[] = [
  {
    title: "Today",
    id: "today",
  },
  {
    title: "Next 3 days",
    id: "within3days",
  },
  {
    title: "Next 2 weeks",
    id: "within2weeks",
    hasBorder: true,
  },
  {
    title: "Telehealth",
    id: "telehealth",
    icon: <VideoVisit />,
  },
  {
    title: "Accepts new patients",
    id: "acceptNew",
  },
];

function getAvailabilityWithCount(
  doctors: Doctor[],
  filters: Availability
): DropdownFilterItem[] {
  return availabilityFilters.map((filter) => {
    const count = doctors.filter((doctor) =>
      patternByAvailabilityFilter[filter.id]?.(doctor)
    )?.length;

    return {
      title: filter.title,
      id: filter.id,
      icon: filter.icon,
      hasBorder: filter.hasBorder,
      count: count || 0,
      checked: filters[filter.id],
    };
  });
}

export const specialityFilter = createSelector(
  [selectDoctors, selectSpecialitiesFilter],
  (doctors, filters) => {
    return getItemsWithCount(doctors, filters, "speciality");
  }
);

export const insuranceFilter = createSelector(
  [selectDoctors, selectInsurancesFilter],
  (doctors, filters) => {
    return getItemsWithCount(doctors, filters, "insurances");
  }
);

export const availabilityFilter = createSelector(
  [selectDoctors, selectAvailabilityFilter],
  (doctors, filters) => {
    return getAvailabilityWithCount(doctors, filters);
  }
);
