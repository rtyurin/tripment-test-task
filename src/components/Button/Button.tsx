import React, { PropsWithChildren } from "react";
import {
  Typography,
  Variant as TypographyVariant,
} from "@/components/Typography/Typography";
import styles from "./Button.module.css";
import clsx from "clsx";

type ButtonVariant = "standard" | "outline";
type ButtonSize = "s" | "m";
function getTitleVariant(
  buttonVariant: ButtonVariant,
  buttonSize: ButtonSize
): TypographyVariant {
  if (buttonVariant === "standard") {
    return "button";
  } else {
    if (buttonSize === "s") {
      return "caption";
    }

    return "body1";
  }
}

export interface ButtonProps {
  variant?: ButtonVariant;
  size?: ButtonSize;
  onClick?: () => void;
}
export const Button = React.memo<PropsWithChildren<ButtonProps>>(
  function Button({
    variant = "standard",
    size = "m",
    children,
    onClick,
  }: PropsWithChildren<ButtonProps>) {
    return (
      <button
        className={clsx(styles.button, {
          [styles[variant]]: variant,
          [styles[size]]: size,
        })}
        onClick={onClick}
      >
        <Typography
          color={variant === "outline" ? "red" : "inherit"}
          variant={getTitleVariant(variant, size)}
          border={variant === "outline" ? "dotted" : undefined}
        >
          {children}
        </Typography>
      </button>
    );
  }
);
