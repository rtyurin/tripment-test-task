import React from "react";
import { useSelector } from "react-redux";
import { filteredDoctorsSelector } from "@/selectors/doctorsSelector";
import { Typography } from "@/components/Typography/Typography";
import { Doctor } from "@/components/Doctor/Doctor";
import styles from "./List.module.css";

export const List = () => {
  const doctorsList = useSelector(filteredDoctorsSelector);

  return (
    <ul className={styles.list}>
      {doctorsList.map((doctor) => (
        <li key={doctor.id}>
          <Doctor doctorInfo={doctor} />
        </li>
      ))}
    </ul>
  );
};
