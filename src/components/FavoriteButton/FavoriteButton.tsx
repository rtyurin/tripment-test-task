import React from "react";
import { HeartIcon } from "@/icons/HeartIcon";
import styles from "./FavoriteButton.module.css";

export const FavoriteButton = () => {
  return (
    <button className={styles.favoriteButton}>
      <HeartIcon />
    </button>
  );
};
