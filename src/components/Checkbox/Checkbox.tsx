import React, { PropsWithChildren, useCallback, useState } from "react";
import styles from "./Checkbox.module.css";
import clsx from "clsx";
import { CheckIcon } from "@/icons/CheckIcon";

interface CheckboxProps {
  onChange: (checked: boolean) => void;
  id: string;
  label?: React.ReactNode;
  checked?: boolean;
}
export const Checkbox = React.memo(function Checkbox({
  onChange,
  id,
  label,
  checked,
}: CheckboxProps) {
  const toggleCheckbox = useCallback(() => {
    onChange(!checked);
  }, []);

  return (
    <label onChange={toggleCheckbox} htmlFor={id} className={styles.label}>
      <input id={id} className={styles.input} type="checkbox" />
      <div
        className={clsx(styles.checkbox, {
          [styles.checkbox_checked]: checked,
        })}
      >
        {checked && <CheckIcon className={styles.checkIcon} />}
      </div>
      {label}
    </label>
  );
});
