import React from "react";
import { VideoVisitIcon12 } from "@/icons/VideoVisitIcon12";
import styles from "./VideoVisit.module.css";

export const VideoVisit = () => (
  <div className={styles.video_visit}>
    <VideoVisitIcon12 />
  </div>
);
