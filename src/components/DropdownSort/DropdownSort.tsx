import React, { useCallback, useRef, useState } from "react";
import { SortIcon } from "@/icons/SortIcon";
import { Typography } from "@/components/Typography/Typography";
import { useClickOutside } from "@/hooks/useClickOutside";
import { RadioGroup, RadioItem } from "@/components/Radio/Radio";
import { useDispatch } from "react-redux";
import {
  sortByAvailabilityAction,
  sortByExperienceAction,
} from "@/reducers/doctorsReducer";
import { ArrowDownIcon } from "@/icons/ArrowDownIcon";
import styles from "./DropdownSort.module.css";

const sortItems: RadioItem[] = [
  {
    id: "byAvailable",
    value: "nextAvailable",
    label: <Typography color={"dark"}>Next Available</Typography>,
  },
  {
    id: "byExperience",
    value: "mostExperienced",
    label: <Typography color={"dark"}>Most experienced</Typography>,
  },
];
const DropdownSortMenu = React.memo(function DropdownSortMenu() {
  const dispatch = useDispatch();
  const handleSortChange = useCallback(
    (value: "nextAvailable" | "mostExperienced") => {
      if (value === "nextAvailable") {
        dispatch(sortByAvailabilityAction());
      } else {
        dispatch(sortByExperienceAction());
      }
    },
    []
  );

  return (
    <div className={styles.menu}>
      <Typography variant={"button"} color={"dark"}>
        Sort by
      </Typography>

      <RadioGroup
        name={"sort"}
        items={sortItems}
        selectedValue={"nextAvailable"}
        onChange={handleSortChange}
      />
    </div>
  );
});

export const DropdownSort = React.memo(function DropdownSort() {
  const [isOpened, setIsOpened] = useState(false);
  const wrapperRef = useRef(null);

  const handleToggleMenu = useCallback(() => {
    setIsOpened((prev) => !prev);
  }, []);

  useClickOutside(wrapperRef, () => {
    setIsOpened(false);
  });

  return (
    <div ref={wrapperRef} className={styles.wrapper}>
      <button onClick={handleToggleMenu} className={styles.button}>
        <SortIcon className={styles.icon} />
        <Typography className={styles.sortTitle} color={"dark"}>
          {" "}
          Sort
        </Typography>
        <ArrowDownIcon fillColor={"#1C383A"} />
      </button>

      {isOpened && <DropdownSortMenu />}
    </div>
  );
});
