import React, { useCallback } from "react";
import styles from "./Input.module.css";
import clsx from "clsx";

interface InputProps {
  value?: string;
  onChange?: (newValue: string) => void;
  append?: React.ReactNode;
  placeholder?: string;
  className?: string;
}
export const Input = ({
  value,
  onChange,
  append,
  placeholder,
  className,
}: InputProps) => {
  const handleChange = useCallback(
    (e) => {
      onChange?.(e?.target?.value || "");
    },
    [onChange]
  );

  return (
    <div className={clsx(styles.wrapper, className)}>
      <input
        placeholder={placeholder}
        value={value}
        onChange={handleChange}
        type="text"
        className={styles.input}
      />
      <div className={styles.append}>{append}</div>
    </div>
  );
};
