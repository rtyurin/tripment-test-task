import React from "react";
import { VideoVisitIcon16 } from "@/icons/VideoVisitIcon16";
import styles from "./Avatar.module.css";

export interface AvatarProps {
  src?: string;
  name?: string;
  hasTelehealth?: boolean;
}
export const Avatar = React.memo(function Avatar({
  src,
  name,
  hasTelehealth,
}: AvatarProps) {
  return (
    <div className={styles.avatar}>
      <img src={src} alt={name} className={styles.img} />
      {hasTelehealth ? (
        <div className={styles.videoVisit}>
          <VideoVisitIcon16 />
        </div>
      ) : null}
    </div>
  );
});
