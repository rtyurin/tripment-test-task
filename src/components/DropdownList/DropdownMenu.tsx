import React, { useCallback, useMemo, useState } from "react";
import { Typography } from "@/components/Typography/Typography";
import { Checkbox } from "@/components/Checkbox/Checkbox";
import clsx from "clsx";
import styles from "./DropdownMenu.module.css";
import { Button } from "@/components/Button/Button";
import { DropdownFilterItem } from "@/selectors/filtersSelector";
import { Input } from "@/components/Input/Input";
import { SearchIcon } from "@/icons/SearchIcon";

interface DropdownMenuProps {
  header?: React.ReactNode;
  items: DropdownFilterItem[];
  size?: "s" | "m" | "l";
  onResetClick: () => void;
  hasSearchField?: boolean;
  title?: string;
  onApplyClick: (selectedIds: number | string[]) => void;
}
export const DropdownMenu = React.memo(function DropdownMenu({
  items,
  header,
  size = "m",
  title,
  onResetClick,
  hasSearchField,
  onApplyClick,
}: DropdownMenuProps) {
  const initialSelected = useMemo(
    () =>
      items.reduce((acc, item) => {
        if (item.checked) {
          return {
            ...acc,
            [item.id]: true,
          };
        }
        return acc;
      }, {}),
    [items]
  );
  const [selected, setSelected] = useState(initialSelected);
  const [search, setSearch] = useState<string>("");
  const filteredList = useMemo(
    () =>
      items.filter((item) =>
        item.title.toLowerCase().match(search.toLowerCase())
      ),
    [items, search]
  );

  const toggleSelect = useCallback(
    (id: string) => () => {
      setSelected((prevState) => ({
        ...prevState,
        [id]: !prevState[id],
      }));
    },
    []
  );

  const handleApplyClick = useCallback(() => {
    const selectedArray = Object.keys(selected);
    const selectedIds = selectedArray.reduce((ids: string[], id: string) => {
      if (selected[id]) {
        return [...ids, id];
      }

      return ids;
    }, []);

    onApplyClick(selectedIds);
  }, [selected]);

  return (
    <div className={styles.wrapper}>
      <div
        className={clsx(styles.menu, {
          [styles[`sizes_${size}`]]: size,
        })}
      >
        {header}

        {hasSearchField && (
          <Input
            className={styles.search}
            placeholder={`Filter by ${title}`}
            append={<SearchIcon />}
            value={search}
            onChange={setSearch}
          />
        )}

        <ul className={styles.list}>
          {filteredList.map((item: DropdownFilterItem) => (
            <li
              className={clsx(styles.listItem, {
                [styles.itemBorder]: item.hasBorder,
              })}
              key={item.id}
            >
              <Checkbox
                onChange={toggleSelect(item.id)}
                checked={selected[item.id]}
                id={item.id}
                label={
                  <>
                    {item.icon && (
                      <div className={styles.icon}>{item.icon}</div>
                    )}
                    <div>
                      <Typography
                        className={styles.itemTitle}
                        color={"dark"}
                        tag={"span"}
                      >
                        {item.title}
                      </Typography>
                      <Typography color={"light"} tag={"span"}>
                        ({item.count})
                      </Typography>
                    </div>
                  </>
                }
              />
            </li>
          ))}
        </ul>
      </div>

      <div className={styles.buttons}>
        <Button variant={"outline"} onClick={onResetClick}>
          Reset
        </Button>

        <Button variant={"standard"} onClick={handleApplyClick}>
          Apply
        </Button>
      </div>
    </div>
  );
});
