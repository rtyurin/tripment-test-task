import React, { useCallback, useMemo, useRef, useState } from "react";
import clsx from "clsx";
import { Typography } from "@/components/Typography/Typography";
import { ArrowDownIcon } from "@/icons/ArrowDownIcon";
import { DropdownMenu } from "@/components/DropdownList/DropdownMenu";
import { useClickOutside } from "@/hooks/useClickOutside";
import { DropdownFilterItem } from "@/selectors/filtersSelector";
import { CrossIcon } from "@/icons/CrossIcon";
import styles from "./DropdownButton.module.css";

export interface DropdownButtonProps {
  title: string;
  menuHeader?: React.ReactNode;
  menuSize?: "m" | "l";
  items: DropdownFilterItem[];
  hasSearchField?: boolean;
  onApplyClick: (selectedIds: string[]) => void;
  onResetClick: () => void;
}
export const DropdownButton = React.memo(function DropdownButton({
  title,
  menuHeader,
  menuSize,
  hasSearchField = false,
  items,
  onApplyClick,
  onResetClick,
}: DropdownButtonProps) {
  const checkedItemsCount = useMemo(
    () => items.filter((i) => i.checked).length,
    [items]
  );
  const [isOpened, setIsOpened] = useState(false);
  const refContainer = useRef<HTMLDivElement>(null);

  useClickOutside(refContainer, () => {
    setIsOpened(false);
  });

  const handleApplyClick = useCallback(
    (ids: string[]) => {
      onApplyClick(ids);
      setIsOpened(false);
    },
    [onApplyClick]
  );

  const handleResetClick = useCallback(() => {
    onResetClick();
    setIsOpened(false);
  }, [onResetClick]);

  const handleResetCrossClick = useCallback(
    (event: React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();

      setIsOpened(false);
      onResetClick();
    },
    [onResetClick]
  );

  const handleToggleDropdown = useCallback(
    () => setIsOpened((prevState) => !prevState),
    []
  );

  return (
    <div className={styles.wrapper} ref={refContainer}>
      <button
        onClick={handleToggleDropdown}
        className={clsx(styles.button, {
          [styles.buttonOpened]: isOpened,
        })}
      >
        <Typography color={isOpened ? "white" : "dark"}>
          {title}
          {checkedItemsCount > 0 && (
            <span
              className={clsx(styles.count, {
                [styles.count_opened]: isOpened,
              })}
            >
              {checkedItemsCount}
            </span>
          )}
        </Typography>
        <div
          className={styles.arrowIcon}
          onClick={checkedItemsCount > 0 ? handleResetCrossClick : undefined}
        >
          {checkedItemsCount > 0 ? (
            <CrossIcon fillColor={isOpened ? "#fff" : undefined} />
          ) : (
            <ArrowDownIcon
              rotate={isOpened ? 180 : 0}
              fillColor={isOpened ? "#fff" : undefined}
            />
          )}
        </div>
      </button>
      {isOpened && (
        <DropdownMenu
          header={menuHeader}
          items={items}
          size={menuSize}
          title={title}
          hasSearchField={hasSearchField}
          onApplyClick={handleApplyClick}
          onResetClick={handleResetClick}
        />
      )}
    </div>
  );
});
