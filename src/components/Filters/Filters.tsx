import React from "react";
import { DropdownButton } from "@/components/DropdownList/DropdownButton";
import { useDispatch, useSelector } from "react-redux";
import {
  availabilityFilter,
  insuranceFilter,
  specialityFilter,
} from "@/selectors/filtersSelector";
import styles from "./Filters.module.css";
import {
  resetAvailability,
  resetFilters,
  resetInsurances,
  resetSpecialities,
  updateAvailability,
  updateInsurances,
  updateSpecialities,
} from "@/reducers/filtersReducer";
import { DropdownSort } from "@/components/DropdownSort/DropdownSort";
import { Button } from "@/components/Button/Button";
import { Typography } from "@/components/Typography/Typography";

export const Filters = () => {
  const specialities = useSelector(specialityFilter);
  const insurances = useSelector(insuranceFilter);
  const availability = useSelector(availabilityFilter);
  const dispatch = useDispatch();

  return (
    <div className={styles.filters}>
      <DropdownButton
        title="Availability"
        items={availability}
        menuSize="m"
        menuHeader={
          <Typography
            variant={"button"}
            color={"dark"}
            tag={"div"}
            className={styles.availabilityTitle}
          >
            Availability
          </Typography>
        }
        onApplyClick={(ids) => dispatch(updateAvailability(ids))}
        onResetClick={() => dispatch(resetAvailability())}
      />

      <DropdownButton
        title="Speciality"
        items={specialities}
        menuSize="l"
        hasSearchField
        onApplyClick={(ids) => dispatch(updateSpecialities(ids))}
        onResetClick={() => dispatch(resetSpecialities())}
      />

      <DropdownButton
        title="Insurance"
        items={insurances}
        menuSize="l"
        hasSearchField
        onApplyClick={(ids) => dispatch(updateInsurances(ids))}
        onResetClick={() => dispatch(resetInsurances())}
      />

      <DropdownSort />

      <Button variant={"outline"} onClick={() => dispatch(resetFilters())}>
        Clear filters
      </Button>
    </div>
  );
};
