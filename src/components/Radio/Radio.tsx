import React, { useCallback, useState } from "react";
import { CheckIcon } from "@/icons/CheckIcon";
import styles from "./Radio.module.css";

interface RadioProps {
  name: string;
  value: string;
  id: string;
  label: React.ReactNode;
  checked?: boolean;
  onChange?: () => void;
}
export const Radio = React.memo(function Radio({
  id,
  name,
  value,
  label,
  onChange,
  checked = false,
}: RadioProps) {
  return (
    <>
      <input
        className={styles.input}
        name={name}
        value={value}
        type="radio"
        checked={checked}
        onChange={onChange}
        id={id}
      />
      <label className={styles.label} htmlFor={id} onChange={onChange}>
        {checked && <CheckIcon className={styles.icon} fillColor="#244D51" />}{" "}
        {label}
      </label>
    </>
  );
});

export interface RadioItem {
  value: string;
  id: string;
  label: React.ReactNode;
}
interface RadioGroupProps {
  items: RadioItem[];
  name: string;
  selectedValue: string;
  onChange?: (value: string) => void;
}
export const RadioGroup = React.memo(function RadioGroup({
  items,
  name,
  selectedValue,
  onChange,
}: RadioGroupProps) {
  const [selected, setSelected] = useState(selectedValue);

  const handleChange = useCallback(
    (value: string) => () => {
      onChange?.(value);
      setSelected(value);
    },
    [onChange]
  );

  return (
    <div>
      {items.map(({ label, id, value }) => (
        <Radio
          checked={selected === value}
          onChange={handleChange(value)}
          key={id}
          label={label}
          value={value}
          name={name}
          id={id}
        />
      ))}
    </div>
  );
});
