import React, { PropsWithChildren } from "react";
import styles from "./Typography.module.css";
import clsx from "clsx";

export type Variant = "body1" | "subtitle" | "h2" | "h3" | "caption" | "button";
type Color = "inherit" | "regular" | "light" | "dark" | "red" | "white";
type Border = "dashed" | "dotted";

export interface TextProps {
  variant?: Variant;
  color?: Color;
  className?: string;
  tag?: keyof JSX.IntrinsicElements;
  border?: Border;
}

function getTagNameByVariant(variant: Variant): keyof JSX.IntrinsicElements {
  switch (variant) {
    case "body1": {
      return "div";
    }
    case "caption": {
      return "span";
    }
    case "button": {
      return "span";
    }
    case "subtitle": {
      return "div";
    }
    case "h2": {
      return "h2";
    }
    case "h3": {
      return "h3";
    }
    default:
      return "div";
  }
}

export const Typography = ({
  variant = "body1",
  color = "inherit",
  tag,
  className,
  border,
  children,
}: PropsWithChildren<TextProps>) => {
  const Tag = tag || getTagNameByVariant(variant);

  return (
    <Tag
      className={clsx(className, {
        [styles[variant]]: variant,
        [styles[color]]: color,
        [styles[border]]: border,
      })}
    >
      {children}
    </Tag>
  );
};
