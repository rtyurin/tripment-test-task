import React from "react";
import { Doctor as DoctorType } from "@/store/store";
import { Avatar } from "@/components/Avatar/Avatar";
import { Typography } from "@/components/Typography/Typography";
import styles from "./Doctor.module.css";
import clsx from "clsx";
import { FavoriteButton } from "@/components/FavoriteButton/FavoriteButton";

export interface DoctorProps {
  doctorInfo: DoctorType;
}

const avatarSrc =
  "https://s3.us-east-2.amazonaws.com/assets.tripment.com/uploads/doctor/avatar/1672/thumb_melissa-cardinal-psychiatric-nurse-practitioner.jpg";

interface SpecialityInfoProps {
  speciality: string;
  experience: number;
  reviewsCount: number;
}
const SpecialityInfo = React.memo(function SpecialityInfo({
  speciality,
  experience,
  reviewsCount,
}: SpecialityInfoProps) {
  return (
    <div className={styles.specialityInfo}>
      <Typography tag={"span"} variant={"body1"}>
        {speciality}
      </Typography>
      {experience && experience > 0 && (
        <Typography tag={"span"} className={styles.dotted}>
          {experience} Years of experience
        </Typography>
      )}
      {reviewsCount && reviewsCount > 1 && (
        <Typography tag={"span"} color={"red"} className={styles.dotted}>
          {reviewsCount} Reviews
        </Typography>
      )}
    </div>
  );
});

interface AddressInfoProps {
  telehealth: boolean;
  address: string;
}
const AddressInfo = React.memo(function AddressInfo({
  address,
  telehealth,
}: AddressInfoProps) {
  return (
    <div className={styles.address}>
      {telehealth && (
        <Typography tag={"span"} color={"light"}>
          Video visit
        </Typography>
      )}
      {address && (
        <Typography
          tag={"span"}
          className={
            telehealth ? clsx(styles.dotted, styles.dottedLight) : null
          }
          color={"light"}
        >
          {address}
        </Typography>
      )}
    </div>
  );
});

interface PriceInfoProps {
  price: number;
}
const PriceInfo = React.memo(function PriceInfo({ price }: PriceInfoProps) {
  return (
    <div className={styles.price}>
      <Typography
        variant={"caption"}
        border={"dotted"}
        tag={"div"}
        className={styles.avgPriceTitle}
      >
        avg. price
      </Typography>

      {price && <Typography variant={"subtitle"}>${price}</Typography>}

      <FavoriteButton />
    </div>
  );
});
export const Doctor = ({ doctorInfo }: DoctorProps) => {
  return (
    <a href="#" className={styles.wrapper}>
      <div className={styles.inner}>
        <Avatar hasTelehealth={doctorInfo.telehealth} src={avatarSrc} />

        <div className={styles.info}>
          <Typography variant={"h3"}>{doctorInfo.name}</Typography>

          <SpecialityInfo
            experience={doctorInfo.experience}
            reviewsCount={doctorInfo.reviewsCount}
            speciality={doctorInfo.speciality}
          />

          <AddressInfo
            address={doctorInfo.address}
            telehealth={doctorInfo.telehealth}
          />
        </div>
      </div>

      <PriceInfo price={doctorInfo.price} />
    </a>
  );
};
