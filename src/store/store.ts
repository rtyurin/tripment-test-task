import { configureStore, EnhancedStore } from "@reduxjs/toolkit";
import { Filters, filtersReducer } from "@/reducers/filtersReducer";
import { doctorsReducer } from "@/reducers/doctorsReducer";

export interface Doctor {
  id: number;
  name: string;
  speciality: string;
  experience: number;
  gender: string;
  reviewsCount: number;
  acceptNew: boolean;
  address: string;
  insurances: string;
  telehealth: boolean;
  telehealth_available: string;
  offline_available: string;
  price: number;
}

export interface Store {
  doctors: {
    items: Doctor[];
    sortBy: "byAvailability" | "byExperience";
  };
  filters: Filters;
}
const store = configureStore({
  reducer: {
    filters: filtersReducer,
    doctors: doctorsReducer,
  },
});

export default store;
