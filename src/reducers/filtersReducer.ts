import { createReducer, createAction } from "@reduxjs/toolkit";

export interface Availability {
  today: boolean;
  within3days: boolean;
  within2weeks: boolean;
  telehealth: boolean;
  acceptNew: boolean;
  schedulesOnline: boolean;
  treatsChildren: boolean;
}

export const updateInsurances = createAction<string[]>("UPDATE_INSURANCES");
export const resetInsurances = createAction<void>("RESET_INSURANCES");
export const updateSpecialities = createAction<string[]>("UPDATE_SPECIALITIES");
export const resetSpecialities = createAction<void>("RESET_SPECIALITIES");
export const updateAvailability = createAction<string[]>("UPDATE_AVAILABILITY");
export const resetAvailability = createAction<void>("RESET_AVAILABILITY");
export const resetFilters = createAction<void>("RESET_FILTERS");

export interface Filters {
  insurances: string[];
  specialities: string[];
  availability: Availability;
}
const initialAvailability: Availability = {
  today: false,
  within3days: false,
  within2weeks: false,
  telehealth: false,
  acceptNew: false,
  schedulesOnline: false,
  treatsChildren: false,
};
const initialState: Filters = {
  insurances: [],
  specialities: [],
  availability: initialAvailability,
};
export const filtersReducer = createReducer<Filters>(
  initialState,
  (builder) => {
    builder
      .addCase(updateInsurances, (state, action) => ({
        ...state,
        insurances: action.payload,
      }))
      .addCase(resetInsurances, (state) => ({
        ...state,
        insurances: [],
      }))
      .addCase(updateSpecialities, (state, action) => ({
        ...state,
        specialities: action.payload,
      }))
      .addCase(resetSpecialities, (state) => ({
        ...state,
        specialities: [],
      }))
      .addCase(updateAvailability, (state, action) => ({
        ...state,
        availability: action.payload.reduce(
          (filters: Availability, newFilter) => {
            return {
              ...filters,
              [newFilter]: true,
            };
          },
          {} as Availability
        ),
      }))
      .addCase(resetAvailability, (state) => ({
        ...state,
        availability: initialAvailability,
      }))
      .addCase(resetFilters, () => initialState);
  }
);
