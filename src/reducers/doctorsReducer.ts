import { createReducer, createAction } from "@reduxjs/toolkit";
import doctorsJson from "../doctors.json";
import { Doctor } from "@/store/store";

export const sortByAvailabilityAction = createAction<void>(
  "SORT_BY_AVAILABILITY"
);
export const sortByExperienceAction = createAction<void>("SORT_BY_EXPERIENCE");

function sortByAvailability(doctors: Doctor[]) {
  return [...doctors].sort((doctor1: Doctor, doctor2: Doctor) => {
    const currentDate = new Date().getTime();
    const distanceAFromCurrentDate = Math.abs(
      currentDate - new Date(doctor1.offline_available).getTime()
    );
    const distanceBFromCurrentDate = Math.abs(
      currentDate - new Date(doctor2.offline_available).getTime()
    );

    return distanceAFromCurrentDate - distanceBFromCurrentDate;
  });
}

function sortByExperience(doctors: Doctor[]) {
  return [...doctors].sort((doctor1, doctor2) => {
    return doctor2.experience - doctor1.experience;
  });
}

const initialState = {
  items: sortByAvailability(doctorsJson.data.items),
  sortBy: "byAvailability",
};

export const doctorsReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(sortByAvailabilityAction, (state) => ({
      sortBy: "byAvailability",
      items: sortByAvailability(state.items),
    }))
    .addCase(sortByExperienceAction, (state) => ({
      sortBy: "byExperience",
      items: sortByExperience(state.items),
    }));
});
