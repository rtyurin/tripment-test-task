import Head from "next/head";
import { List } from "@/components/DoctorsList/List";
import { Typography } from "@/components/Typography/Typography";
import { InfoIcon } from "@/icons/InfoIcon";
import { MainLayout } from "@/layout/MainLayout/MainLayout";
import { Filters } from "@/components/Filters/Filters";
import styles from "@/styles/Home.module.css";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Tripment Test Task</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@800&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap"
          rel="stylesheet"
        />
      </Head>

      <MainLayout>
        <Filters />
        <Typography className={styles.title} variant={"h2"} color={"regular"}>
          Root Canal doctors in New York, NY
        </Typography>
        <div className={styles.discretion}>
          <InfoIcon />
          <Typography className={styles.discretionText}>
            The average price of a procedure in New York is $300
          </Typography>
        </div>
        <List />
      </MainLayout>
    </div>
  );
}
