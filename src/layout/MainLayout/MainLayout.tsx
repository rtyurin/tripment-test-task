import React, { PropsWithChildren } from "react";
import styles from "./MainLayout.module.css";

export const MainLayout = React.memo(
  ({ children }: PropsWithChildren<Record<string, unknown>>) => {
    return (
      <div className={styles.outerLayout}>
        <div className={styles.innerLayout}>{children}</div>
      </div>
    );
  }
);
